﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns_BuildPatterns.Structural.Adapter
{
    public class BabushkasTV
    {
        public void ConnectToThinSocket()
        {
            Console.WriteLine("Babushkas TV is connected by thin socket");
        }
    }
}
