﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns_BuildPatterns.Structural.Adapter
{
    public class NewTV : IDevice
    {
        public void ConnectToWideSocket()
        {
            Console.WriteLine("New TV is connected to wide socket");
        }
    }
}
