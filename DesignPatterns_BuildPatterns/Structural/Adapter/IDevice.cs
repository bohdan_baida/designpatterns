﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns_BuildPatterns.Structural.Adapter
{
    public interface IDevice
    {
        void ConnectToWideSocket();
    }
}
