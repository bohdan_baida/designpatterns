﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns_BuildPatterns.Structural.Adapter
{
    public class TVAdapter : IDevice
    {
        BabushkasTV babushkasTV;

        public TVAdapter(BabushkasTV babushkasTV)
        {
            this.babushkasTV = babushkasTV;
        }

        public void ConnectToWideSocket()
        {
            babushkasTV.ConnectToThinSocket();
            Console.WriteLine("Adapter connected by wide socket");
        }
    }
}
