﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns_BuildPatterns.Structural.Facade
{
    public class HotelService
    {
        public void BookHotel(int stars)
        {
            Console.WriteLine($"Hotel with {stars} stars booked");
        }
    }
}
