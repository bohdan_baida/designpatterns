﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns_BuildPatterns.Structural.Facade
{
    public class JoinUpFacade
    {
        Airlines airlines;
        HotelService hotelService;
        CateringService cateringService;

        public JoinUpFacade()
        {
            airlines = new Airlines();
            hotelService = new HotelService();
            cateringService = new CateringService(); 
        }

        public void HaveAllInclusiveRest(FlightType flightType, CateringType cateringType, int hotelLevel)
        {
            airlines.BookFlight(flightType);
            hotelService.BookHotel(hotelLevel);
            cateringService.BookCatering(cateringType);
        }

        public void HaveWildRest(FlightType flightType)
        {
            airlines.BookFlight(flightType);
        }

    }
}
