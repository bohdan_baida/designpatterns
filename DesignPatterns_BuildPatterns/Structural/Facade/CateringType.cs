﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns_BuildPatterns.Structural.Facade
{
    public enum CateringType
    {
        AI = 1,
        ThreeTimesADay,
        UltraAI
    }
}
