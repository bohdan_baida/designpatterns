﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns_BuildPatterns.Structural.Facade
{
    public enum FlightType
    {
        Econom = 1,
        Comfort,
        Business,
        LikeAGod
    }
}
