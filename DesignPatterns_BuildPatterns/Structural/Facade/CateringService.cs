﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns_BuildPatterns.Structural.Facade
{
    public class CateringService
    {
        public void BookCatering(CateringType cateringType)
        {
            Console.WriteLine($"Catering program of type {Enum.GetName(typeof(CateringType), cateringType)} selected");
        }
    }
}
