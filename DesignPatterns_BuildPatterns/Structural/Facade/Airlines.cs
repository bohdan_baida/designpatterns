﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns_BuildPatterns.Structural.Facade
{
    public class Airlines
    {
        public void BookFlight(FlightType flightType)
        {
            Console.WriteLine($"Flight of  {Enum.GetName(typeof(FlightType), flightType)} class booked");
        }
    }
}
