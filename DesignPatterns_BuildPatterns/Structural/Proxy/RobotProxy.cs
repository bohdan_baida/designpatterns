﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns_BuildPatterns.Structural.Proxy
{
    public class RobotProxy : IRobot
    {
        private Robot robot;

        public RobotProxy(Robot robot)
        {
            this.robot = robot;
        }

        public void TurnLeft()
        {
            Console.WriteLine("[BlackBox] Left");
            robot.TurnLeft();
        }

        public void TurnRight()
        {
            Console.WriteLine("[BlackBox] Right");
            robot.TurnRight();
        }

        public void Go()
        {
            Console.WriteLine("[BlackBox] Go");
            robot.Go();
        }

        public void Back()
        {
            Console.WriteLine("[BlackBox] Back");
            robot.Back();
        }

        public void KickYourFriend()
        {
            Console.WriteLine("[BlackBox] Kick");
            robot.KickYourFriend();
        }
    }
}
