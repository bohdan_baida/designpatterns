﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns_BuildPatterns.Structural.Proxy
{
    public class Robot : IRobot
    {
        public void TurnLeft()
        {
            Console.WriteLine("Turned left");
        }

        public void TurnRight()
        {
            Console.WriteLine("Turned right");
        }

        public void Go()
        {
            Console.WriteLine("Go");
        }

        public void Back()
        {
            Console.WriteLine("Back");
        }

        public void KickYourFriend()
        {
            Console.WriteLine("Kick was sucessful");
        }
    }
}
