﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns_BuildPatterns.Structural.Proxy
{
    public interface IRobot
    {
        void TurnLeft();
        void TurnRight();
        void Go();
        void Back();
        void KickYourFriend();
    }
}
