﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns_BuildPatterns.Structural.Wrapper
{
    public class CarWrapper:Car
    {
        private Car car;

        public Car Car => car;

        public CarWrapper(Car car)
        {
            this.car = car;
        }

        public override void Go()
        {
            car.Go();
            Console.WriteLine("beep beep beep");
        }
    }
}
