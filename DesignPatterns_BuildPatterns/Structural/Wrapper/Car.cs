﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns_BuildPatterns.Structural.Wrapper
{
    public class Car
    {
        public virtual string Model => "Model not defined";

        public virtual void Go()
        {
            Console.WriteLine("bzz");
        }
    }
}
