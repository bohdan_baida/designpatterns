﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns_BuildPatterns.Structural.Wrapper
{
    public class NextCarWrapper: Car
    {
        private Car car;

        public Car Car => car;

        public NextCarWrapper(Car car)
        {
            this.car = car;
        }

        public override void Go()
        {
            car.Go();
            Console.WriteLine("next next next");
        }
    }
}
