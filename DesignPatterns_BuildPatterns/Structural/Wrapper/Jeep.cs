﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns_BuildPatterns.Structural.Wrapper
{
    public class Jeep : Car
    {
        public override string Model => "Jeep car";

        public override void Go()
        {
            Console.WriteLine("brrr");
        }
    }
}
