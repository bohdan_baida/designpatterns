﻿using DesignPatterns_BuildPatterns.Creational.FactoryMethod;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns_BuildPatterns.Creational.Singleton
{
    public class Logger
    {
        private static Logger logger;

        private LoggerEnum loggingType = LoggerEnum.Dev;

        public static Logger Instanse
        {
            get
            {
                return logger == default(Logger) 
                    ? logger = new Logger() 
                    : logger;
            }
        }

        private Logger()
        {
        }

        public void Log()
        {
            Console.WriteLine($"Something was logged by logger {loggingType}");
        }

        public void ChangeLoggingType(LoggerEnum loggerType)
        {
            loggingType = loggerType;
        }




    }
}
