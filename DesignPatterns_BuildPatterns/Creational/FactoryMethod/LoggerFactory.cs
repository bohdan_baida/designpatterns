﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns_BuildPatterns.Creational.FactoryMethod
{
    public static class LoggerFactory
    {
        public static ILogger GetLogger(LoggerEnum logger)
        {
            switch (logger)
            {
                case LoggerEnum.Dev:
                    return new DevLogger();
                case LoggerEnum.Prod:
                    return new ProdLogger();
                default:
                    throw new InvalidEnumArgumentException(nameof(logger), (int)logger, typeof(LoggerEnum));
            }
        }
    }
}
