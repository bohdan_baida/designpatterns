﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns_BuildPatterns.Creational.FactoryMethod
{
    public class DevLogger : ILogger
    {
        public void Log()
        {
            Console.WriteLine("Dev logger logged something");
        }
    }
}
