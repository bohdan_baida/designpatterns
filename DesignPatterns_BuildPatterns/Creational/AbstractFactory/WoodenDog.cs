﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns_BuildPatterns.Creational.AbstractFactory
{
    public class WoodenDog : Dog
    {
        public override string Name => "WoodenDog";

        public override void Gav()
        {
            Console.WriteLine("Gaw! I am a wooden good guy");
        }
    }
}
