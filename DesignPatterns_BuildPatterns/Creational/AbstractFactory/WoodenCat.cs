﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns_BuildPatterns.Creational.AbstractFactory
{
    public class WoodenCat : Cat
    {
        public override string Name => "WoodenCat";

        public override void Mew()
        {
            Console.WriteLine("Mew! I am a wooden cat");
        }
    }
}
