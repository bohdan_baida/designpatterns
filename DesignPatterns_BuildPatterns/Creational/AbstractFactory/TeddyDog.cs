﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns_BuildPatterns.Creational.AbstractFactory
{
    public class TeddyDog : Dog
    {
        public override string Name => "TeddyDog";

        public override void Gav()
        {
            Console.WriteLine("Gaw! I am a teddy good guy");
        }
    }
}
