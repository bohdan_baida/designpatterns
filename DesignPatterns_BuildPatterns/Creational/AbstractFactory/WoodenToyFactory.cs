﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns_BuildPatterns.Creational.AbstractFactory
{
    public class WoodenToyFactory : IToyFactory
    {
        public Cat GetCat()
            => new WoodenCat();

        public Dog GetDog()
            => new WoodenDog();
    }
}
