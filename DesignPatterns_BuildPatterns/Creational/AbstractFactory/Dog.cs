﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns_BuildPatterns.Creational.AbstractFactory
{
    public abstract class Dog
    {
        public abstract string Name { get; }

        public abstract void Gav();
    }
}
