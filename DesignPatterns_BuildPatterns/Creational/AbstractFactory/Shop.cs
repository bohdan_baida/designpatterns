﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns_BuildPatterns.Creational.AbstractFactory
{
    public class Shop
    {
        List<Cat> cats;
        List<Dog> dogs;

        public Shop(IToyFactory toyFactory)
        {
            cats = new List<Cat>();
            dogs = new List<Dog>();
            for (int i = 0; i < 10; i++)
            {
                cats.Add(toyFactory.GetCat());
                dogs.Add(toyFactory.GetDog());
            }
        }

        public Cat GetCat()
        {
            return cats.First();
        }
        public Dog GetDog()
        {
            return dogs.First();
        }
    }
}
