﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns_BuildPatterns.Creational.AbstractFactory
{
    public class TeddyCat : Cat
    {
        public override string Name => "TeddyCat";

        public override void Mew()
        {
            Console.WriteLine("Mew! I am a Teddy Cat");
        }
    }
}
