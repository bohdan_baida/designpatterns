﻿using DesignPatterns_BuildPatterns.Behaivor.ChainOfResponsibility;
using DesignPatterns_BuildPatterns.Behaivor.Command;
using DesignPatterns_BuildPatterns.Behaivor.TemplateMethod;
using DesignPatterns_BuildPatterns.Creational.AbstractFactory;
using DesignPatterns_BuildPatterns.Creational.FactoryMethod;
using DesignPatterns_BuildPatterns.Creational.Singleton;
using DesignPatterns_BuildPatterns.Structural.Adapter;
using DesignPatterns_BuildPatterns.Structural.Facade;
using DesignPatterns_BuildPatterns.Structural.Proxy;
using DesignPatterns_BuildPatterns.Structural.Wrapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns_BuildPatterns
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Abstract factory
            //IToyFactory factory;
            //factory = new TeddyToyFactory();

            //var shop = new Shop(factory);

            //var cat = shop.GetCat();
            //cat.Mew();
            //var dog = shop.GetDog();
            //dog.Gav();
            #endregion

            #region Factory method
            //var loggerType = (LoggerEnum)int.Parse(System.Configuration.ConfigurationManager.AppSettings.Get("logger"));

            //var logger = LoggerFactory.GetLogger(loggerType);

            //logger.Log();
            #endregion

            #region Singleton
            //Logger.Instanse.Log();

            //Logger.Instanse.ChangeLoggingType(LoggerEnum.Prod);

            //Logger.Instanse.Log();
            #endregion

            #region Adapter
            //IDevice device;

            ////device = new NewTV();

            //device = new TVAdapter(new BabushkasTV());
            //device.ConnectToWideSocket();
            #endregion

            #region Proxy
            //IRobot robot = new RobotProxy(new Robot());
            //robot.Go();
            //robot.TurnLeft();
            //robot.Go();
            //robot.KickYourFriend();
            //robot.Back();
            #endregion

            #region Facade
            //JoinUpFacade joinUp = new JoinUpFacade();

            //joinUp.HaveAllInclusiveRest(FlightType.LikeAGod, CateringType.UltraAI, 5);
            //Console.WriteLine("_");

            //joinUp.HaveAllInclusiveRest(FlightType.Comfort, CateringType.AI, 4);
            //Console.WriteLine("_");

            //joinUp.HaveWildRest(FlightType.Econom);
            #endregion

            #region Wrapper
            //var jeep = new Jeep();
            //jeep.Go();

            //var jeepWrapper = new CarWrapper(jeep);
            //jeepWrapper.Go();

            ////var oldJeep = jeepWrapper.Car;
            ////oldJeep.Go();

            //var nextWrapper = new NextCarWrapper(jeepWrapper);
            //nextWrapper.Go();
            #endregion

            #region Chain Of Responsibility
            //var letter = new EmailLetter()
            //{
            //    From = "bober0071997@gmail.com",
            //    CC = new List<string>(),
            //    Header = "[party]"
            //};
            //EmailFilterSystem.ProcessLetter(letter);
            #endregion

            #region Command
            //var logger = new LoggerService();
            //logger.AddLogger(new TcpLogger());
            //logger.AddLogger(new DBLogger());
            //logger.AddLogger(new ConsoleLogger());

            ////logger.LogSuccess(nameof(Program.Main));
            //logger.LogFail(nameof(Program.Main),"something went wrong");

            #endregion

            #region Template method
            var reports = new List<ReportGenerator>()
            {
                new MedicalGoodsReportGenerator(),
                new FoodReportGenerator(),
                new TechnicsReportGenerator()
            };
            foreach (var report in reports)
            {
                var reportResult = report.Generate();
                Console.WriteLine($"{Enum.GetName(typeof(GoodTypeEnum),reportResult.GoodName)} | {reportResult.TotalPrice} | {reportResult.CustomersQuantity} | {reportResult.TotalQuantity}");
            }
            #endregion
            Console.Read();
        }
    }
}
