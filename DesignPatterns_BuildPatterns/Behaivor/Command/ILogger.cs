﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns_BuildPatterns.Behaivor.Command
{
    public interface ILogger
    {
        void LogSuccess(string method);
        void LogFail(string method, string error);

    }
}
