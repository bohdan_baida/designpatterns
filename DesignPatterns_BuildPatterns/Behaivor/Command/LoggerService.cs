﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns_BuildPatterns.Behaivor.Command
{
    public class LoggerService : ILogger
    {
        List<ILogger> loggers;

        public LoggerService()
        {
            loggers = new List<ILogger>();
        }

        public void AddLogger(ILogger logger)
        {
            loggers.Add(logger);
        }

        public void LogFail(string method, string error)
        {
            foreach (var logger in loggers)
            {
                logger.LogFail(method, error);
            }
        }

        public void LogSuccess(string method)
        {
            foreach (var logger in loggers)
            {
                logger.LogSuccess(method);
            }
        }
    }
}
