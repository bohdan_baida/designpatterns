﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns_BuildPatterns.Behaivor.Command
{
    public class TcpLogger : ILogger
    {
        public void LogFail(string method, string error)
        {
            Console.WriteLine($"[Tcp]:Fail {method}|{error}");
        }

        public void LogSuccess(string method)
        {
            Console.WriteLine($"[Tcp]:Success {method}");
        }
    }
}
