﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns_BuildPatterns.Behaivor.Command
{
    public class DBLogger : ILogger
    {
        public void LogFail(string method, string error)
        {
            Console.WriteLine($"[DB]:Fail {method}|{error}");
        }

        public void LogSuccess(string method)
        {
            Console.WriteLine($"[DB]:Success {method}");
        }
    }
}
