﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns_BuildPatterns.Behaivor.ChainOfResponsibility
{
    public class BugReportHandler : EmailHandler
    {
        public BugReportHandler(EmailHandler handler)
            : base(handler)
        {

        }

        public override void ProcessLetter(EmailLetter letter)
        {
            if (letter.Header.Contains("[BugReport]"))
            {
                Console.WriteLine("Letter was sent to development team");
                return;
            }
            base.ProcessLetter(letter);
        }
    }
}
