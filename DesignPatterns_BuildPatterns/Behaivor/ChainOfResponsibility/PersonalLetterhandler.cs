﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns_BuildPatterns.Behaivor.ChainOfResponsibility
{
    public class PersonalLetterHndler : EmailHandler
    {
        public PersonalLetterHndler(EmailHandler handler)
            : base(handler)
        {

        }

        public override void ProcessLetter(EmailLetter letter)
        {
            Console.WriteLine("Personal letter was delivered for you");
        }
    }
}
