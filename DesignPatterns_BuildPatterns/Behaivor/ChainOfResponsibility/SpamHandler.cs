﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns_BuildPatterns.Behaivor.ChainOfResponsibility
{
    public class SpamHandler : EmailHandler
    {
        public SpamHandler(EmailHandler handler)
            : base(handler)
        {

        }

        public override void ProcessLetter(EmailLetter letter)
        {
            if (!TrustedEmails.IsTrusted(letter.From))
            {
                Console.WriteLine("Letter moved to spam!");
                return;
            }

            base.ProcessLetter(letter);
        }
    }
}
