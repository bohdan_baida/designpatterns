﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns_BuildPatterns.Behaivor.ChainOfResponsibility
{
    public static class EmailFilterSystem
    {
        static EmailHandler handler;

        static EmailFilterSystem()
        {
            handler = new SpamHandler(
                new CorporativeMailHandler(
                    new BugReportHandler(
                        new PersonalLetterHndler(null))));
        }

        public static void ProcessLetter(EmailLetter letter)
        {
            handler.ProcessLetter(letter);
        }
    }
}
