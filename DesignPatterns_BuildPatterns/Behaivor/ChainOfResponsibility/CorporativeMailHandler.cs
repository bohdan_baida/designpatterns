﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns_BuildPatterns.Behaivor.ChainOfResponsibility
{
    public class CorporativeMailHandler : EmailHandler
    {
        public CorporativeMailHandler(EmailHandler handler)
            : base(handler)
        {

        }

        public override void ProcessLetter(EmailLetter letter)
        {
            if (letter.CC.Any())
            {
                Console.WriteLine("Email was forwarded to CEO!");
                return;
            }

            base.ProcessLetter(letter);
        }
    }
}
