﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns_BuildPatterns.Behaivor.ChainOfResponsibility
{
    public class EmailHandler
    {
        private EmailHandler nextHandler { get; set; }

        public EmailHandler(EmailHandler nextHandler)
        {
            this.nextHandler = nextHandler;
        }

        public virtual void ProcessLetter(EmailLetter letter)
        {
            nextHandler.ProcessLetter(letter);
        }
    }
}
