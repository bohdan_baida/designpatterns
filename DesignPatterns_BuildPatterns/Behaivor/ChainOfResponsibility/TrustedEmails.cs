﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns_BuildPatterns.Behaivor.ChainOfResponsibility
{
    public static class TrustedEmails
    {
        private static IEnumerable<string> trustedMails;

        static TrustedEmails()
        {
            trustedMails = new List<string>()
            {
                "bober0071997@gmail.com",
                "lida.kolodiy12@gmail.com",
                "ruslan.baida@gmail.com",
                "alex.skurzh@gmail.com"
            };
        }

        public static bool IsTrusted(string email)
        {
            return trustedMails.Contains(email);
        }
    }
}
