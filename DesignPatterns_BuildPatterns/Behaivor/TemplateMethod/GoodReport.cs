﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns_BuildPatterns.Behaivor.TemplateMethod
{
    public class GoodReport
    {
        public GoodTypeEnum GoodName { get; set; }
        public int CustomersQuantity { get; set; }
        public int TotalQuantity { get; set; }
        public decimal TotalPrice { get; set; }
    }
}
