﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns_BuildPatterns.Behaivor.TemplateMethod
{
    public class TechnicsReportGenerator : ReportGenerator
    {
        protected override void SetCustomersQuantity(GoodReport report)
        {
            report.CustomersQuantity = 21;
        }

        protected override void SetName(GoodReport report)
        {
            report.GoodName = GoodTypeEnum.Technics;
        }

        protected override void SetTotalPrice(GoodReport report)
        {
            report.TotalPrice = 7865;
        }

        protected override void SetTotalQuantity(GoodReport report)
        {
            report.TotalQuantity = 23;
        }
    }
}
