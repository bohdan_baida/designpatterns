﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns_BuildPatterns.Behaivor.TemplateMethod
{
    public abstract class ReportGenerator
    {
        protected abstract void SetName(GoodReport report);

        protected abstract void SetTotalQuantity(GoodReport report);

        protected abstract void SetTotalPrice(GoodReport report);

        protected abstract void SetCustomersQuantity(GoodReport report);

        public GoodReport Generate()
        {
            var result = new GoodReport();

            SetName(result);
            SetTotalPrice(result);
            SetTotalQuantity(result);
            SetCustomersQuantity(result);

            return result;
        }
    }
}
