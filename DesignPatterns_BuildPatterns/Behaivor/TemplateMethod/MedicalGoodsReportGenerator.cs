﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns_BuildPatterns.Behaivor.TemplateMethod
{
    class MedicalGoodsReportGenerator : ReportGenerator
    {
        protected override void SetCustomersQuantity(GoodReport report)
        {
            report.CustomersQuantity = 100;
        }

        protected override void SetName(GoodReport report)
        {
            report.GoodName = GoodTypeEnum.Medical;
        }

        protected override void SetTotalPrice(GoodReport report)
        {
            report.TotalPrice = 212321.22m;
        }

        protected override void SetTotalQuantity(GoodReport report)
        {
            report.TotalQuantity = 2121;
        }
    }
}
