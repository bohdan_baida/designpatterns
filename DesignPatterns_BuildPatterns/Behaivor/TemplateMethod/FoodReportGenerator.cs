﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns_BuildPatterns.Behaivor.TemplateMethod
{
    public class FoodReportGenerator : ReportGenerator
    {
        protected override void SetCustomersQuantity(GoodReport report)
        {
            report.CustomersQuantity = 2211;
        }

        protected override void SetName(GoodReport report)
        {
            report.GoodName = GoodTypeEnum.Food;
        }

        protected override void SetTotalPrice(GoodReport report)
        {
            report.TotalPrice = 23321;
        }

        protected override void SetTotalQuantity(GoodReport report)
        {
            report.TotalQuantity = 12;
        }
    }
}
